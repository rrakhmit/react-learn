import {Link} from 'react-router-dom';

function Test() {
    return (

      <div className="container">
          <h1>I am Test Page</h1>
          <Link to="/" className="btn btn-info">Back to Home Page</Link>
      </div>

    );
  }
  
  export default Test;