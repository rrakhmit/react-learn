import Slider from './Home/Slider';
import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

function Body({name, address, count}) {

    let v = '';
    for(let i=0; i<count; i++){
        v += `${name} - `; 
    }

    let HOST = 'https://api.themoviedb.org/3/';
    let API_KEY = 'bfee40d1d3a0bb92a118c339232b22cd';

    const [trendingMovies, settrendingMovies] = useState({});
    const [isLoading, setIsLoading] = useState(0);

    useEffect(()=>{

      const loadTrending = async ()=>{

        await axios.get(`${HOST}/trending/movie/week?api_key=${API_KEY}`)
        .then(res => {
            settrendingMovies(res.data);
            setIsLoading(1);
        });

        /*await fetch(`${HOST}/trending/movie/week?api_key=${API_KEY}`)
        .then(response => response.json())
        .then(data => settrendingMovies(data) );
        setIsLoading(1);*/
      }

      loadTrending();

      
    }, []);

    function RenderMovie(){

      if(isLoading>0){
        return (
          trendingMovies.results.map((movie)=>{
            let imgSrc = `http://image.tmdb.org/t/p/original${movie.poster_path}`;
            let detailsPath = `/movie/${movie.id}`;
            return (
              <div className="col" key={movie.id}>
                <div className="card shadow-sm">
                  <img src={imgSrc} className="card-img-top" alt={movie.title} width="100%"/>

                  <div className="card-body">
                    <p className="card-text">{movie.title}</p>
                    <div className="d-flex justify-content-between align-items-center">
                      <div className="btn-group">
                        <Link to={detailsPath} className="btn btn-sm btn-outline-secondary">View</Link>
                      </div>
                      <small className="text-muted">9 mins</small>
                    </div>
                  </div>
                </div>
              </div>
            )
          })
        )
      }else{
        return ('0');
      }
    }

    return (
      <main>

        <Slider/>

        <div className="album py-5 bg-light">
          <div className="container">

            <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">

            {
                <RenderMovie />
            }


              
             
                
          </div>
          </div>
        </div>

      </main>
    );


  }
  
  export default Body;