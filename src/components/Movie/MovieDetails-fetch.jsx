import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';

function MovieDetails({match}) {

    const [movieDetals, setmovieDetails] = useState({});
    const [isLoading, setIsLoading] = useState(0);

    let HOST = 'https://api.themoviedb.org/3/';
    let API_KEY = 'bfee40d1d3a0bb92a118c339232b22cd';

    useEffect(()=>{

        const loadMovie = async ()=>{
          await fetch(`${HOST}/movie/${match.params.id}?api_key=${API_KEY}`)
          .then(response => response.json())
          .then(data => setmovieDetails(data) );
          setIsLoading(1);
        }
  
        loadMovie();
  
        
    }, []);

    function RenderMovieDetails(){

        if(isLoading>0){

            let backdrop = `http://image.tmdb.org/t/p/original${movieDetals.backdrop_path}`;
            let poster = `http://image.tmdb.org/t/p/original${movieDetals.poster_path}`;

            return (

                <div className="col">
                    <div className="card shadow-sm">
                    <img src={backdrop} className="card-img-top" alt={movieDetals.title} width="100%"/>

                    <div className="card-body">
                        <p className="card-text">{movieDetals.title}</p>
                        <div className="d-flex justify-content-between align-items-center">
                        <div className="btn-group">
                            
                        </div>
                        <small className="text-muted">9 mins</small>
                        </div>
                    </div>
                    </div>
                    <Link to="/" className="btn btn-sm btn-outline-secondary">Back</Link>
                </div>
                

            )
        }else{
            return ('0');
        }
    }

    return (
        <RenderMovieDetails/>
    );
}
  
  export default MovieDetails;