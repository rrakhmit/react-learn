function Logo({width}) {
    return (
      <div className="Logo">
        <img src="https://www.coder71.com/upload/logo/1554027736527.png" alt="Coder71 Ltd." width={width}/>
      </div>
    );
  }
  
  export default Logo;