// import './App.css';
//import {useEffect} from 'react';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
// ./css/bootstrap.min.css
import Hheader from './components/Hheader';
import Body from './components/Body';
import Footer from './components/Footer';
import Test from './components/Test';
import Home from './components/Home';
import MovieDetails from './components/Movie/MovieDetails';

import {Nav, Toast} from '../node_modules/bootstrap/dist/js/bootstrap.bundle';

import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';


function App() {

  let name = 'Coder71 Ltd.'
  let address = 'Dhaka'
  let count = 5
  

  document.title = "This is the new page title.";

  return (
    <Router>
    <div className="App">
      
      <Hheader />

      
        <Switch>

          <Route path="/" exact>
            <Body name={name} address={address} count={count}/>
          </Route>

          <Route path="/movie/:id" exact component={MovieDetails}/>

          <Route path="/test" exact component={Test} />

        </Switch>
      

      {/* <Body name={name} address={address} count={count}/> */}

      <Footer/>
      

      <div className="toast-container position-absolute top-0 end-0 p-3">
        <div className="toast" role="alert" aria-live="polite" aria-atomic="true">
          <div className="toast-header">
            <strong className="me-auto">Bootstrap</strong>
            <small>11 mins ago</small>
            <button type="button" className="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
          </div>
          <div className="toast-body">
            Hello, world! This is a toast message.
          </div>
        </div>
      </div>

    </div>
    </Router>
  );

}

export default App;

/*Array.from(document.querySelectorAll('.toast'))
    .forEach(toastNode => new Toast(toastNode));*/

document.querySelector("body").addEventListener("click", function(){ 
  var el = document.querySelector(".toast");
  el = new Toast(el);
  el.show();
});
